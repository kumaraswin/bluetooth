//
//  BluetoothService.swift
//  SampleBluetooth
//
//  Created by Sreejith K Menon on 04/05/20.
//  Copyright © 2020 Sreejith K Menon. All rights reserved.
//

import Foundation
import CoreBluetooth

class BluetoothService: NSObject, BluetoothSerialDelegate {
    
    static let sharedInstance = BluetoothService()
    
    var commandsDict: [String:String] =
        ["Stop":"403",
         "RightHandShake":"406",
         "LeftHandShake":"405"]
    
    override init() {
        super.init()
        serial = BluetoothSerial(delegate: self)
    }
    
    func connect() {
        serial.startScan()
    }
    
    func disconnect() {
        serial.disconnect()
    }
    
    func serialDidDiscoverPeripheral(_ peripheral: CBPeripheral, RSSI: NSNumber?) {
        serial.connectToPeripheral(peripheral)
        serial.stopScan()
    }
    
    func serialDidChangeState() {
        
    }
    
    func serialDidDisconnect(_ peripheral: CBPeripheral, error: NSError?) {
        
    }
    
    func sendMessage(text: String) {
        let message = "\(text)"
        serial.sendMessageToDevice("d\(message)\r\n")
    }
    
    func sendCommandToRobot(command: String) {
        if !commandsDict[command]!.isEmpty {
            sendMessage(text: commandsDict[command]!)
            print("Command found")
        } else {
            print("Command not found")
        }
    }
    
    func serialDidReceiveString(_ message: String) {
        
    }
    
    func serialDidReceiveBytes(_ bytes: [UInt8]) {
        
    }
    
    func serialDidReceiveData(_ data: Data) {
        
    }
    
    func serialDidConnect(_ peripheral: CBPeripheral) {
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) {_ in
            self.sendCommandToRobot(command: "RightHandShake")
        }
    }
}
